import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  // 存放数据的
  state: {
    sum: 0,
    school: '美术学院',
    subject: '前端',
    personList: [{ id: '001', name: '张三' }]
  },
  getters: {
    bigSum(state) {
      return state.sum * 10
    }
  },
  // 修改state状态的
  mutations: {
    JIA(state, value) {
      console.log('mutations中的JIA被调用了')
      state.sum += value
    },
    JIAN(state, value) {
      console.log('mutations中的JIAN被调用了')
      state.sum -= value
    },
    ADD_PERSON(state, value) {
      console.log('mutations中的ADD_PERSON被调用了')
      state.personList.unshift(value)
    }
  },
  // 通过commit发送给mutations
  actions: {
    jiaOdd(context, value) {
      console.log('actions中的jiaOdd被调用了')
      if (context.state.sum % 2) {
        context.commit('JIA', value)
      }
    },
    jiaWait(context, value) {
      console.log('actions中的jiaWait被调用了')
      setTimeout(() => {
        context.commit('JIA', value)
      }, 500)
    }
  },
  modules: {}
})
