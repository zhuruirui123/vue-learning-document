import Vue from 'vue'
import App from './App.vue'
import router from './router'

// 引入vuex
// import Vuex from 'vuex'

import store from './store'
// 关闭vue的生产提示
Vue.config.productionTip = false

// 使用插件
// Vue.use(Vuex)

new Vue({
  router,
  render: h => h(App),
  store
  // 安装全局事件总线
  // beforeCreate() {
  //   Vue.prototype.$bus = this
  // }
}).$mount('#app')
