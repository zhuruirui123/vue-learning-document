import Vue from 'vue'
import VueRouter from 'vue-router'
// 引入路由组件
import Home from '../components/pages/Home.vue'
import About from '../components/pages/About.vue'
import Message from '../components/pages/Message.vue'
import News from '../components/pages/News.vue'
import Detail from '../components/pages/Detail.vue'

// 使用路由
Vue.use(VueRouter)

// 配置路由规则
const routes = [
  { path: '/home', component: Home },
  {
    path: '/about',
    component: About,
    children: [
      { path: 'news', component: News },
      {
        path: 'message',
        component: Message,
        children: [
          {
            name: 'message-detail',
            // path: 'detail/:id/:title',
            path: 'detail',
            component: Detail,
            // 第一种写法：props值为对象，该对象中所有的key-value的组合最终都会通过props传给Detail组件
            // 当 props 是静态的时候很有用
            // props: { a: 1, b: 2 }
            // 第二种写法：props值为布尔值，布尔值为true，则把路由收到的所有params参数通过props传给Detail组件
            // props: true
            // 第三种写法：props值为函数，返回对象中所有的key-value的组合最终都会通过props传给Detail组件
            props($route) {
              return {
                id: $route.query.id,
                title: $route.query.title
              }
            }
          }
        ]
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
