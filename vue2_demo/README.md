# 封装过渡和动画

1. test 文件是利用动画 animation 属性来实现动画
2. test2 文件是利用过渡 transition 属性来实现过渡的
3. test3 文件是安装的第三方样式库 animate.css 使用上面的样式来实现动画的

# GitHub 案例

1. 使用 axios 向后台请求数据，使用 eventBus 实现兄弟之间的通信（首先在入口文件中定义全局事件总线 beforeCreate，然后使用$bus定义\$emit触发事件和\$on 监听事件），然后将数据渲染到页面上；
2. 展示用户列表 users
3. **优化**：当用户列表没有展示出来之前，可以先让页面展示欢迎词 isFirst 和加载中 isLoading 以及出错信息 errMsg
4. 当点击搜索按钮之后，请求的数据还没渲染到页面之前，isFirst 为 false，isLoading 为 true，errMsg 为空，users 数组为空
5. 请求成功，isFirst 为 false，isLoading 为 false，users 数组不为空
6. 发送请求之前，更新 list 组件的数据 this.$bus.\$emit()
7. 将 data 数据包装成对象，事件回调函数使用赋值解构和...展开运算符更新 data 数据
